﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    public float speed = 5;
    public float jumpForce = 10;

    Rigidbody rb;
    Camera cam;

    float rotX;
    float rotY;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        cam = Camera.main;
        Cursor.lockState = CursorLockMode.Locked;
    }
    
    void Update()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        float mousex = Input.GetAxis("Mouse X");
        float mousey = Input.GetAxis("Mouse Y");

        if (Input.GetButtonDown("Jump"))
        {
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
        }

        rotY += mousex;
        rotX += mousey;

        rotX = Mathf.Clamp(rotX, -90, 90);

        rb.velocity = new Vector3(0, rb.velocity.y, 0) + (transform.forward * v + transform.right * h) * speed;

        transform.rotation = Quaternion.Euler(0, rotY, 0);
        cam.transform.localRotation = Quaternion.Euler(-rotX, 0, 0);
    }
}
